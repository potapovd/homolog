//= plugins/jquery-3.3.1.min.js
//= plugins/bootstrap/bootstrap.min.js
//= plugins/aos.min.js
AOS.init();
$(document).ready(function () {




    $(".megamenu").on("click", function(e) {
        e.stopPropagation();
    });



    $('#navbar .dropdown').hover(function() {
        //console.log($(this).find('.dropdown-menu a').html());

        $(this).find('.dropdown-menu a').each( function( index, element ){
            //console.log( $( this ).addClass('olo') );

            (function(that, i) {
                var t = setTimeout(function() {
                    $(that).addClass("active-visible");
                    //$(that).show();
                }, 20 * i);
            })(this, index);


        });


        //$(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
    }, function() {
        $('.dropdown-menu a').removeClass("active-visible")
        //$(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp()
    });


    $(window).scroll(function(){
        var scrollPos = $(document).scrollTop();
        if(scrollPos>10){
            $("#header").addClass("scrolled")
        }else{
            $("#header").removeClass("scrolled")
        }
        //console.log(scrollPos);
    });

    $('.menuicon').on('click',function (e) {
        e.stopPropagation();
        //console.log('1')
        $('.menuline3,.menuline2-1').toggle();
        //$('#nav-icon2').toggleClass('open');
    });
    $("#desctopcartbutton").on('click',function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.menucart .megamenu').toggle();
    });
    $("#desctopcabtbutton").on('click',function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.menucabinet .megamenu').toggle();
    })

    $('#cartmenu a').on('click',function (e) {
        e.preventDefault();
        var id = $(this).attr('data-link');
        var page = $(this).attr('data-page');
        $('.pagenumber span').html(page);
        $('#cartmenu a').removeClass("active")
        $(this).toggleClass("active")
        $('.slides').addClass("hidden").removeClass('show')
        $("#"+id+"").addClass('show')

    })
    // $('#nav-icon2').click(function(){
    //
    // });


    window.onload = function () {
        // if (window.jQuery) {
        // 	// jQuery is loaded
        // 	alert("Yeah!");
        // } else {
        // 	// jQuery is not loaded
        // 	alert("Doesn't Work");
        // }
    }


});